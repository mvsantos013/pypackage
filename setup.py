import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="mypypackage", # Replace with your own username
    version="0.0.1",
    author="Mateus Santos",
    author_email="mateus.santos@hashdex.com",
    description="A package containing utilities functions",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://bitbucket.org/mvsantos013/pypackage",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.6',
)